
const routes = [
  {
    path: '/',
    name: '/',
    redirect: '/dashboard'
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('layouts/NoDrawer.vue'),
    children: [
      { path: '', component: () => import('pages/Login/Login.vue') },
    ],
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('layouts/Default.vue'),
    children: [
      { path: '', component: () => import('pages/Dashboard.vue') },
    ],
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;
