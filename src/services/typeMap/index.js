const IQA = 'IQA';
const PH = 'PH';
const TDS = 'TDS';
const TURB = 'TURB';
const typeMap = {

  state() {
    return{
      type: '',
    };
  },
  mutations: {
    [IQA] (state) {
      const localStorage = state;
      localStorage.type = 'IQA';
    },
    [PH] (state) {
      const localStorage = state;
      localStorage.type = 'PH';
    },
    [TDS] (state) {
      const localStorage = state;
      localStorage.type = 'TDS';
    },
    [TURB] (state) {
      const localStorage = state;
      localStorage.type = 'TURB';
    },
  },
  actions: {
    Iqua({commit}){
      commit(IQA);
    },
    Ph({commit}){
      commit(PH);
    },
    Tds({commit}){
      commit(TDS);
    },
    Turb({commit}){
      commit(TURB);
    },
  }
}

export default typeMap;