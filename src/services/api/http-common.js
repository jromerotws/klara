import axios from 'axios';

const HTTP = axios.create({
  baseURL: 'http://' + window.location.hostname + ":3000/",
});

export default HTTP;
