import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
import auth from '../services/auth/index';
import typeMap from '../services/typeMap/index'

Vue.use(Vuex);
const vuexLocalStorage = new VuexPersist({
  key: 'vuex',
  storage: localStorage,
  modules: ['auth', 'typeMap'],
});

const store = new Vuex.Store({
  plugins: [vuexLocalStorage.plugin],
  modules: {
    auth,
    typeMap,
  },
});

export default store;
