FROM node:10-slim

RUN apt-get update -qq && \
    apt-get install -y build-essential git

COPY . /klara
WORKDIR /klara

CMD ["sh", "start.sh"]
